package net.ciuak.lambder

// The Value trait -- representation of any value in the program.
class Value(val fn: (Expression, Value.Env) => Value) {

  // Call with an empty environment.
  final def call(arg: Expression): Value = call(arg, Map())

  // Call the value with an argument and the environment.
  def call(arg: Expression, env: Map[String, Value]): Value = fn(arg, env)

  override def toString = "*value*"

}

object Value {
  type Env = Map[String, Value]
  def apply(fn: (Expression, Value.Env) => Value) = new Value(fn)
}

// Literal is a symbol. Symbols sometimes have a special meaning.
class Literal(val s: String) extends Value(null) {

  // Numbers' methods
  lazy val number_methods = Map[String, Value](
    "+" -> new Value((e, env) => new Literal((e.eval(env).asInstanceOf[Literal].asInt.get + asInt.get).toString))
  )

  // Do stuff with the symbol.
  override def call(arg: Expression, env: Map[String, Value]): Value = {
    var supported_arguments = Map[String, Value]()
    if(asInt.isDefined)
      supported_arguments = supported_arguments ++ number_methods
    val res = arg.eval(env).asInstanceOf[Literal]
    supported_arguments(res.s)
  }

  // Tries to extract an integer from `s`. Returns an option.
  protected def asInt: Option[Int] =
    "[-+]?[0-9]+".r.anchored.findFirstIn(s).map(_.toInt)

  override def toString = "*literal \"" + s + "\"*"

}