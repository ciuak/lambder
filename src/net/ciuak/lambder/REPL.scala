package net.ciuak.lambder

import java.util.Scanner

object REPL extends App {
  val sc = new Scanner(System.in)
  while(true) {
    System.out.print("< ")
    val in   = sc.nextLine()
    val toks = Tokenizer(in)
    val exp  = Parser(toks)
    val`val` = exp.eval()
    System.out.println("> " + `val`.toString)
  }
}