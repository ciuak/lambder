package net.ciuak.lambder

// The Abstract Syntax Tree (AST) representation of the program.
abstract class Expression {

  // Evaluate with an empty environment.
  final def eval(): Value = eval(Map())

  // Evaluate the expression with the given environment.
  def eval(env: Map[String, Value]): Value

}

// A constant value.
case class Constant(v: Value) extends Expression {

  // Return the stored value itself.
  override def eval(env: Map[String, Value]): Value = v

}

case class Call(receiver: Expression, argument: Expression) extends Expression {

  // Evaluate the expression with the given environment.
  override def eval(env : Map[String, Value]): Value = receiver.eval(env).call(argument, env)

}