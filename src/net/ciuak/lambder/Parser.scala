package net.ciuak.lambder

object Parser {

  def findClosingParenthesis(tokens: Seq[Tokenizer.Token]): Int = {
    var depth = 1; var index = 0
    while(index < tokens.length) {
      tokens(index) match {
        case Tokenizer.Literal(_) =>
        case Tokenizer.ClosingParenthesis() =>
          depth -= 1
          if(depth == 0)
            return index
        case Tokenizer.OpeningParenthesis() =>
          depth += 1
      }
      index += 1
    }
    null
  }

  // The parser method -- accepts a list of tokens as the parameter and outputs an Expression.
  def apply(tokens: Seq[Tokenizer.Token]): Expression = {
    var exp: Expression = null
    var tok = tokens
    while(tok.length > 0) {
      val t = tok.head
      t match {
        case Tokenizer.ClosingParenthesis() =>
          throw new NegativeArraySizeException("3 < 4")
        case Tokenizer.Literal(s) =>
          if(exp == null)
            exp = Constant(new Literal(s))
          else
            exp = Call(exp, Constant(new Literal(s)))
          tok = tok.tail
        case Tokenizer.OpeningParenthesis() =>
          val i = findClosingParenthesis(tok.tail)
          exp = Call(exp, Parser(tok.tail.take(i)))
          tok = tok.drop(i + 2)
      }
    }
    exp
  }

}
