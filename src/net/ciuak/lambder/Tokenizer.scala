package net.ciuak.lambder

import scala.collection.mutable

object Tokenizer {
  
  // Defines the tokens (not a lot of them)
  trait Token
  case class OpeningParenthesis() extends Token
  case class ClosingParenthesis() extends Token
  case class Literal(s: String)   extends Token
  
  // The tokenizer method -- accepts a string as the parameter and outputs a list of tokens
  def apply(string: String): Array[Token] = {
    var in = string.trim
    val out = mutable.Stack[Token]()
    while(in.length > 0) {
      
      // Match parentheses
      if(in.head == '(') {
        in = in.tail
        out.push(OpeningParenthesis())
      } else if(in.head == ')') {
        in.tail
        out.push(ClosingParenthesis())
      }

      // If nothing matches assume a symbol
      else {
        // Match everything except parentheses and spaces and push a symbol -- this part cannot fail
        val `match` = "^[^() ]+".r.findFirstIn(in).get
        in = in.drop(`match`.length)
        out.push(Literal(`match`))
      }
      
      in.trim // Skip spaces
      
    }
    out.toArray
  }

}