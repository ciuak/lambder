# Lambder
A language about lambdas.

### ???
This language uses symbols as data and concatenation as the only operation.
Thus addition "2 + 5" becomes a call of function "2 +" with "5",
and "2 +" is itself a call of "2" with "+".

### Features
Nothing yet.
